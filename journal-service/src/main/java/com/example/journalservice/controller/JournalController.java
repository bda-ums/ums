package com.example.journalservice.controller;

import com.example.journalservice.model.dto.request.JournalRequest;
import com.example.journalservice.model.dto.response.JournalResponse;
import com.example.journalservice.service.IJournal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/journal")
public class JournalController {

    private final IJournal iJournal;

    public JournalController(IJournal iJournal) {
        this.iJournal = iJournal;
    }

    @GetMapping("/test")
    public String test() {
        return "test";
    }

    @GetMapping("/all")
    public List<JournalResponse> getAllJournals() {
        return iJournal.getAllJournals();
    }

    @PostMapping()
    public void createJournal(JournalRequest journalRequest) {
        iJournal.createJournal(journalRequest);
    }
}

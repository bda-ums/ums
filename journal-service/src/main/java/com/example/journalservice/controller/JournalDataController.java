package com.example.journalservice.controller;

import com.example.journalservice.model.dto.request.JournalDataRequest;
import com.example.journalservice.model.dto.response.JournalDataResponse;
import com.example.journalservice.service.IJournalData;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/journaldata")
public class JournalDataController {

    private final IJournalData iJournalData;

    public JournalDataController(IJournalData iJournalData) {
        this.iJournalData = iJournalData;
    }

    @GetMapping("/all")
    public List<JournalDataResponse> getAllJournalData() {
        return iJournalData.getAllJournalsData();
    }

    @PostMapping
    public void createJournalData(@RequestBody JournalDataRequest journalDataRequest) {
        iJournalData.createJournalData(journalDataRequest);
    }

}

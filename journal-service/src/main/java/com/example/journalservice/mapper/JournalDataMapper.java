package com.example.journalservice.mapper;


import com.example.journalservice.model.dto.request.JournalDataRequest;
import com.example.journalservice.model.dto.response.JournalDataResponse;
import com.example.journalservice.model.entity.JournalDataEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface JournalDataMapper {

    JournalDataResponse mapJournalDataEntityToJournalDataResponse(JournalDataEntity journalDataEntity);
    @Mapping(target = "id", ignore = true)
    JournalDataEntity mapJournalDataRequestToJournalDataEntity(JournalDataRequest journalDataRequest);



}

package com.example.journalservice.mapper;


import com.example.journalservice.model.dto.request.JournalRequest;
import com.example.journalservice.model.dto.response.JournalResponse;
import com.example.journalservice.model.entity.JournalEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface JournalMapper {

    JournalResponse mapJournalEntityToJournalResponse(JournalEntity journalEntity);

    @Mapping(target = "id", ignore = true)
    JournalEntity mapJournalRequestToJournalEntity(JournalRequest journalRequest);


}

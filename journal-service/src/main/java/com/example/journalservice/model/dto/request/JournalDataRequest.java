package com.example.journalservice.model.dto.request;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JournalDataRequest {
    Long groupId;
    Long journalId;
    Long teacherId;
    Long studentId;
    String date;
    String attendance;
}

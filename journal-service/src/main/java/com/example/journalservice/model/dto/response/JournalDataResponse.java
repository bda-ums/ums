package com.example.journalservice.model.dto.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JournalDataResponse {
    Long id;
    Long groupId;
    Long journalId;
    Long teacherId;
    Long studentId;
    String date;
    String attendance;
}

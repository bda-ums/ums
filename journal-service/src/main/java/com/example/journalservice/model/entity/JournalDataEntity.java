package com.example.journalservice.model.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "journal_data")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JournalDataEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "group_id")
    Long groupId;

    @Column(name = "journal_id")
    Long journalId;

    @Column(name = "teacher_id")
    Long teacherId;

    @Column(name = "student_id")
    Long studentId;

    @Column(name = "date")
    String date;

    @Column(name = "attendance")
    String attendance;
}

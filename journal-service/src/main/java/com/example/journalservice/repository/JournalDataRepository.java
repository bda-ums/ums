package com.example.journalservice.repository;

import com.example.journalservice.model.entity.JournalDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JournalDataRepository extends JpaRepository<JournalDataEntity, Long> {
}

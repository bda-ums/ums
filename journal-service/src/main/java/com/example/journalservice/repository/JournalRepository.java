package com.example.journalservice.repository;

import com.example.journalservice.model.entity.JournalEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JournalRepository extends JpaRepository<JournalEntity, Long> {
}

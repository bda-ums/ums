package com.example.journalservice.service;

import com.example.journalservice.model.dto.request.JournalRequest;
import com.example.journalservice.model.dto.response.JournalResponse;

import java.util.List;

public interface IJournal {

    void createJournal(JournalRequest journalRequest);

    List<JournalResponse> getAllJournals();
}

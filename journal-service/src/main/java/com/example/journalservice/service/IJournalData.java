package com.example.journalservice.service;

import com.example.journalservice.model.dto.request.JournalDataRequest;
import com.example.journalservice.model.dto.response.JournalDataResponse;

import java.util.List;

public interface IJournalData {

    void createJournalData(JournalDataRequest journalDataRequest);

    List<JournalDataResponse> getAllJournalsData();
}

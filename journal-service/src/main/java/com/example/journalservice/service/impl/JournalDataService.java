package com.example.journalservice.service.impl;

import com.example.journalservice.mapper.JournalDataMapper;
import com.example.journalservice.model.dto.request.JournalDataRequest;
import com.example.journalservice.model.dto.response.JournalDataResponse;
import com.example.journalservice.repository.JournalDataRepository;
import com.example.journalservice.service.IJournalData;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class JournalDataService implements IJournalData {

    private final JournalDataRepository repository;
    private final JournalDataMapper mapper;

    public JournalDataService(JournalDataRepository repository, JournalDataMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public void createJournalData(JournalDataRequest journalDataRequest) {
        repository.save(mapper.mapJournalDataRequestToJournalDataEntity(journalDataRequest));
    }

    @Override
    public List<JournalDataResponse> getAllJournalsData() {
        return repository.findAll()
                .stream().map(mapper::mapJournalDataEntityToJournalDataResponse)
                .collect(Collectors.toList());
    }
}

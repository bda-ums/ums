package com.example.journalservice.service.impl;

import com.example.journalservice.mapper.JournalMapper;
import com.example.journalservice.model.dto.request.JournalRequest;
import com.example.journalservice.model.dto.response.JournalResponse;
import com.example.journalservice.repository.JournalRepository;
import com.example.journalservice.service.IJournal;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class JournalService implements IJournal {

    private final JournalRepository repository;
    private final JournalMapper mapper;

    public JournalService(JournalRepository repository, JournalMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public void createJournal(JournalRequest journalRequest) {
        repository.save(mapper.mapJournalRequestToJournalEntity(journalRequest));
    }

    @Override
    public List<JournalResponse> getAllJournals() {
        return repository.findAll()
                .stream().map(mapper::mapJournalEntityToJournalResponse)
                .collect(Collectors.toList());
    }
}

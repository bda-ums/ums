package com.example.notificationservice.constant;

import org.springframework.stereotype.Component;

@Component
public class MailText {

  private static String confirmationLink = "http://localhost:8082/main-auth/confirm/";




    public String getLink(String otp) {
        confirmationLink += otp;

        String FIRST_EMAIL_TEXT = "<html><body><p>Please click the following link to confirm your account:</p>"
                + "<a href=\"" + confirmationLink + "\">Confirm Account</a>"
                + "</body></html>";

        String SECOND_EMAIL_TEXT = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "  <meta charset=\"UTF-8\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "  <title>Email Verification</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "  <table style=\"width: 100%; max-width: 600px; margin: 0 auto; font-family: Arial, sans-serif; border-collapse: collapse;\">\n" +
                "    <tr>\n" +
                "      <td style=\"padding: 20px; text-align: center; background-color: #01b8f0b6;\">\n" +
                "        <h2>Email Verification</h2>\n" +
                "      </td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td style=\"padding: 20px;\">\n" +
                "        <p>Hello,</p>\n" +
                "        <p>Thank you for signing up. Please click the link below to verify your email address:</p>\n" +
                "        <p><a href=\"" + confirmationLink + "\" style=\"display: inline-block; padding: 10px 20px; background-color: #006eff; color: #fff; text-decoration: none; border-radius: 5px;\">Verify Email</a></p>\n" +
                "        <p>If you didn't sign up for our service, you can safely ignore this email.</p>\n" +
                "      </td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td style=\"padding: 20px; text-align: center; background-color: #01b8f0b6;\">\n" +
                "        <p>&copy; 2023 Detroit University</p>\n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </table>\n" +
                "</body>\n" +
                "</html>\n";


        System.out.println(confirmationLink + "getlinkdeki confirmation");
        System.out.println(SECOND_EMAIL_TEXT + "SECOND EMAIL TEXT");
        return SECOND_EMAIL_TEXT;
    }

    public String getLink(String otp, String username, String password) {
        confirmationLink += otp;

        String EMAIL_TEXT_WITH_PASSWORD = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "  <meta charset=\"UTF-8\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "  <title>Login Credentials</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "  <table style=\"width: 100%; max-width: 600px; margin: 0 auto; font-family: Arial, sans-serif; border-collapse: collapse;\">\n" +
                "    <tr>\n" +
                "      <td style=\"padding: 20px; text-align: center; background-color: #01b8f0b6;\">\n" +
                "        <h2>Login Credentials</h2>\n" +
                "      </td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td style=\"padding: 20px;\">\n" +
                "        <p>Hello,</p>\n" +
                "        <p>Your login credentials are as follows:</p>\n" +
                "        <p><strong>Username:</strong> \" "+ username +"\"</p>\n" +
                "        <p><strong>Password:</strong>  \""+ password + "\"</p>\n" +
                "        \n" +
                "               <p><a href=\"" + confirmationLink +"\" style=\"display: inline-block; padding: 10px 20px; background-color: #006eff; color: #fff; text-decoration: none; border-radius: 5px;\">Verify Email</a></p>\n" +
                "        <p>For security reasons, we recommend changing your password after your first login.</p>\n" +
                "      </td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td style=\"padding: 20px; text-align: center; background-color: #01b8f0b6;\">\n" +
                "        <p>&copy; 2023 Detroit University</p>\n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </table>\n" +
                "</body>\n" +
                "</html>\n";

        return EMAIL_TEXT_WITH_PASSWORD;
    }

}

package com.example.notificationservice.constants;

public class Message {

    public static String SUCCESFULLY_SENT_EMAIL_MESSAGE = "Email uğurla göndərildi";
    public static String TASK_ADD_MESSAGE = "Sizin üçün yeni task əlavə edildi";

    public static String TASK_DELETE_MESSAGE = "Sizin üçün ayrılmış task silindi";
    public static String TASK_DONE_MESSAGE = "Əlavə etdiyiniz task həll edildi";

    public static String TEACHER_SUCCESSFULLY_REGISTERED_MESSAGE = "UMS saytında Müəllim olaraq qeydiyyatdan keçdiniz";

    public static String STUDENT_SUCCESSFULLY_REGISTERED_MESSAGE = "UMS saytında Tələbə olaraq qeydiyyatdan keçdiniz";

    public static String ADMIN_SUCCESSFULLY_REGISTERED_MESSAGE = "UMS saytında Admin olaraq qeydiyyatdan keçdiniz";

}

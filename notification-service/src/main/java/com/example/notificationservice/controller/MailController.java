package com.example.notificationservice.controller;

import com.example.notificationservice.model.MailStructure;
import com.example.notificationservice.model.response.ConfirmationMailRequest;
import com.example.notificationservice.service.MailService;
import org.springframework.web.bind.annotation.*;

import static com.example.notificationservice.constants.Message.SUCCESFULLY_SENT_EMAIL_MESSAGE;

@RestController
@RequestMapping("/mail")
public class MailController {

    private final MailService mailService;

    public MailController(MailService mailService) {
        this.mailService = mailService;
    }

    @PostMapping("/send/{mail}")
    public String sendMail(@PathVariable String mail, @RequestBody MailStructure mailStructure) {
        mailService.sendMail(mail,mailStructure);
        return SUCCESFULLY_SENT_EMAIL_MESSAGE;
    }

    @PostMapping("/send/confirm-mail")
    public void sendConfirmMail(@RequestBody ConfirmationMailRequest confirmationMailRequest) {
        mailService.sendMail(confirmationMailRequest);
    }

    @PostMapping("/test")
    public String test(@RequestBody String name) {
        System.out.println(name + "name-mail-method");
        return name;
    }
}

package com.example.notificationservice.service;

import com.example.notificationservice.constant.MailText;
import com.example.notificationservice.model.MailStructure;
import com.example.notificationservice.model.response.ConfirmationMailRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
public class MailService {
    protected JavaMailSender mailSender;
    private final MailText mailText;

//    public MailService(JavaMailSender mailSender) {
//        this.mailSender = mailSender;
//    }


    public MailService(JavaMailSender mailSender, MailText mailText) {
        this.mailSender = mailSender;
        this.mailText = mailText;
    }

    @Value("${spring.mail.username}")
    private String fromMail;

    public void sendMail(String mail, MailStructure mailStructure) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(fromMail);
        simpleMailMessage.setSubject(mailStructure.getSubject());
        simpleMailMessage.setText(mailStructure.getMessage());
        simpleMailMessage.setTo(mail);

        mailSender.send(simpleMailMessage);
    }

    public ResponseEntity<String> sendMail(ConfirmationMailRequest request) {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, "utf-8");
        String text = mailText.getLink(request.getOtp(), request.getUsername(), request.getPassword());
        System.out.println(text);

        try {
            helper.setTo(request.getEmail());
            helper.setSubject("Confirm account!");


            helper.setText(text, true);

            mailSender.send(message);

            return new ResponseEntity<>("Email sent successfully.", HttpStatus.OK);
        } catch (Exception e) {
            throw new RuntimeException("Email could not be sent");
        }
    }
}

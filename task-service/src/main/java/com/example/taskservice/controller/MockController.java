package com.example.taskservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MockController {

    @GetMapping("/test")
    public ResponseEntity<String> testMethod () {
        return new ResponseEntity<>("Test method ise dusdu", HttpStatus.OK);
    }
}

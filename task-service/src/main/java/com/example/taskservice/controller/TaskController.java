package com.example.taskservice.controller;

import com.example.taskservice.model.dto.TaskDTO;
import com.example.taskservice.service.ITaskService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/add")
    public TaskDTO addTask(@RequestBody TaskDTO taskDTO) {
        return taskService.add(taskDTO);
    }

    @GetMapping("/all")
    public List<TaskDTO> getAllTasks () {
        return taskService.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskDTO> getTaskById (@PathVariable Long id) {
        return new ResponseEntity<>(taskService.get(id), HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<TaskDTO> updateTask (@RequestBody TaskDTO taskDTO) {
        return new ResponseEntity<>(taskService.update(taskDTO), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<TaskDTO> deleteTask (@PathVariable Long id) {
        taskService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

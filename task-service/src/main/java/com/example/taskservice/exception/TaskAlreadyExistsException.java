package com.example.taskservice.exception;

public class TaskAlreadyExistsException extends RuntimeException{
    private static final long serialVersionUID = 2;

    public TaskAlreadyExistsException(String message) {
        super(message);
    }
}

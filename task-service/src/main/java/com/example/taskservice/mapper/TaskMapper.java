package com.example.taskservice.mapper;

import com.example.taskservice.model.dto.TaskDTO;
import com.example.taskservice.model.entity.Task;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE,componentModel = "spring")
public interface TaskMapper {
    TaskMapper MAPPER = Mappers.getMapper(TaskMapper.class);
    TaskDTO mapToTaskDTO(Task task);
    Task mapToTask(TaskDTO taskDTO);
}

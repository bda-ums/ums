package com.example.taskservice.model.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskDTO {

    Long id;

    Long teacherId;

    Long groupId;

    String title;

    String description;

}

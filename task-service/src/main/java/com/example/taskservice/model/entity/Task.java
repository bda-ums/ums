package com.example.taskservice.model.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "task")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Task extends Base {

    @Column(name = "teacher_id")
    Long teacherId;

    @Column(name = "group_id")
    Long groupId;

    @Column(name = "title")
    String title;

    @Column(name = "description",length = 65450, columnDefinition = "text")
    String description;

/*  Further plans!
    @Column(name = "task_status")
    boolean taskStatus;*/

    @OneToOne(mappedBy = "task", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    TaskInfo taskInfo;

/*     Under maintenance!
    @PrePersist
    protected void onCreate() {
        if (taskInfo == null) {
            taskInfo = new TaskInfo();
        }
    }*/

}

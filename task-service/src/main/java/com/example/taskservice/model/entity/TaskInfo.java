package com.example.taskservice.model.entity;

import lombok.*;
import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "task_info")
public class TaskInfo extends Auditable {

//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
//    @Column(name = "creation_date")
//    LocalDateTime creationDate;

//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
//    @Column(name = "last_update_date")
//    LocalDateTime updateDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "id")
    Task task;

    public Task getTask() {
        return task;
    }

//    public Task getTasks() {
//        return task;
//    }
//
//    public void setTask(Task task) {
//        this.task = task;
//    }
}

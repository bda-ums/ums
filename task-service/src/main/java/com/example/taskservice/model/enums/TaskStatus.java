package com.example.taskservice.model.enums;

public enum TaskStatus {
    IN_PROGRESS,
    DONE,
    UNCHECKED
}

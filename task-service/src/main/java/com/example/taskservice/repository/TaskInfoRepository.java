package com.example.taskservice.repository;

import com.example.taskservice.model.entity.TaskInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskInfoRepository extends JpaRepository<TaskInfo, Long> {
}

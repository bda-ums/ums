package com.example.taskservice.repository;

import com.example.taskservice.model.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long> {
    Optional<Task> findByTitle(String title);
    Optional<Task> findByTitleAndGroupId(String title, Long groupId);
}

package com.example.taskservice.service;

import java.util.List;

public interface ICrudService<DTO, IdType> {

    DTO add(DTO dto);

    List<DTO> getAll();

    DTO get(IdType id);

    DTO update(DTO dto);

    void delete(IdType id);
}

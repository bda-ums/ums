package com.example.taskservice.service;

import com.example.taskservice.model.dto.TaskDTO;

import java.util.List;

public interface ITaskService extends ICrudService<TaskDTO, Long>{

    TaskDTO add(TaskDTO taskDTO);
    List<TaskDTO> getAll();
    TaskDTO get(Long id);
    TaskDTO update(TaskDTO taskDTO);
}

package com.example.taskservice.service.impl;

import com.example.taskservice.exception.TaskAlreadyExistsException;
import com.example.taskservice.exception.TaskNotFoundException;
import com.example.taskservice.mapper.TaskMapper;
import com.example.taskservice.model.dto.TaskDTO;
import com.example.taskservice.model.entity.Task;
import com.example.taskservice.model.entity.TaskInfo;
import com.example.taskservice.repository.TaskRepository;
import com.example.taskservice.service.ITaskService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TaskServiceImpl implements ITaskService {

    private final TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public TaskDTO add(TaskDTO taskDTO) {
        /*      Thinking on new implementation for finding same task from list.
        var existingTaskTitle = taskRepository.findByTitle(taskDTO.getTitle());
        var existingTaskGroupId = taskRepository.findById(taskDTO.getId());*/

        String title = taskDTO.getTitle();
        Long groupId = taskDTO.getGroupId();

        Optional<Task> existingTask = taskRepository.findByTitleAndGroupId(title, groupId);

        if (existingTask.isPresent()) {
            throw new TaskAlreadyExistsException("Task with the same title in the given group already exists!");
        }

        Task task = TaskMapper.MAPPER.mapToTask(taskDTO);
        TaskInfo taskInfo = new TaskInfo();
        taskInfo.setTask(task);
        task.setTaskInfo(taskInfo);
        Task savedTask = taskRepository.save(task);

        return TaskMapper.MAPPER.mapToTaskDTO(savedTask);
    }

    @Override
    public List<TaskDTO> getAll() {
        List<Task> tasks = taskRepository.findAll();

        return tasks.stream().map((TaskMapper.MAPPER::mapToTaskDTO)
        ).collect(Collectors.toList());
    }

    @Override
    public TaskDTO get(Long id) {
        return taskRepository.findById(id)
                .map(TaskMapper.MAPPER::mapToTaskDTO)
                .orElseThrow(() -> new TaskNotFoundException("Task not found while getting it by id!"));
    }


    @Override
    public TaskDTO update(TaskDTO taskDTO) {
        // Creates new object inside table rather than updating existing one! Problem has to be solved!
        // My assumption is that if checker creates new object because it is null.
        Task existingTaskMain = taskRepository.findById(taskDTO.getId())
                .orElseThrow(() -> new TaskNotFoundException("Task not found while updating!"));

        Long groupId = existingTaskMain.getGroupId();
        Long teacherId = existingTaskMain.getTeacherId();

            TaskInfo taskInfo = existingTaskMain.getTaskInfo();
            existingTaskMain = TaskMapper.MAPPER.mapToTask(taskDTO);
            taskInfo.setLastModifiedDate(LocalDateTime.now());
            existingTaskMain.setTaskInfo(taskInfo);
            existingTaskMain.setGroupId(groupId);
            existingTaskMain.setTeacherId(teacherId);

        taskRepository.save(existingTaskMain);
        return TaskMapper.MAPPER.mapToTaskDTO(existingTaskMain);
    }

    @Override
    public void delete(Long id) {
        Optional<Task> existingTask = taskRepository.findById(id);
        if (existingTask.isEmpty()) {
            throw new TaskNotFoundException("Task not found while trying to delete it!");
        }
        existingTask.ifPresent(task -> taskRepository.deleteById(task.getId()));
    }
}

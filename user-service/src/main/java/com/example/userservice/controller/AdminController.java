package com.example.userservice.controller;

import com.example.userservice.model.dto.request.*;
import com.example.userservice.model.dto.response.AdminResponse;
import com.example.userservice.model.dto.response.GroupResponse;
import com.example.userservice.model.dto.response.StudentResponse;
import com.example.userservice.model.dto.response.TeacherResponse;
import com.example.userservice.service.*;
import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class AdminController {

    private final IAdminService adminService;
    private final IStudentService studentService;
    private final IRoleService roleService;
    private final ITeacherService teacherService;
    private final IGroupService groupService;


    /**
     * GET-ALL METHODS
     */
    @GetMapping("/admins")
    public List<AdminResponse> getAdmins() {
        return adminService.getAdmins();
    }

    @GetMapping("/teachers")
    public List<TeacherResponse> getAllTeachers() {
        return teacherService.getAllTeachers();
    }

    @GetMapping("/students")
    public List<StudentResponse> getAllStudents() {
        return studentService.getAllStudents();
    }


    @GetMapping("/groups")
    public List<GroupResponse> getAllGroups() {
        return groupService.getAllGroups();
    }


    /**
     * ADD METHODS
     */

    @PostMapping("/add-role")
    public void createRole(@RequestBody @Valid CreateRoleRequest createRoleRequest){
        roleService.createRole(createRoleRequest);
    }
    @PostMapping("/add-admin")
    public void createAdmin(@RequestBody @Valid CreateAdminRequest request) {
        adminService.createAdmin(request);
    }

    @PostMapping("/add-teacher")
    public void createTeacher(@RequestBody @Valid CreateTeacherRequest createTeacherRequest){
        teacherService.createTeacher(createTeacherRequest);
    }
    @PostMapping("/add-student")
    public void createStudent(@RequestBody @Valid CreateStudentRequest createStudentRequest){
        studentService.createStudent(createStudentRequest);
    }

    @PostMapping("/add-group")
    public void createGroup(@RequestBody @Valid CreateGroupRequest createGroupRequest) {
        groupService.createGroup(createGroupRequest);
    }


    /**
     * UPDATE METHODS
     */

    @PutMapping("/{id}/update-teacher")
    public void updateTeacher(@PathVariable Long id, @RequestBody UpdateTeacherRequest updateTeacherRequest) {
        teacherService.updateTeacher(id, updateTeacherRequest);
    }


    @PutMapping("/{id}/update-student")
    public void updateStudent(@PathVariable Long id, @RequestBody UpdateStudentRequest updateStudentRequest){
        studentService.updateStudent(id, updateStudentRequest);
    }

}


package com.example.userservice.controller;


import com.example.userservice.model.dto.request.UpdateGroupRequest;
import com.example.userservice.model.dto.response.GroupResponse;
import com.example.userservice.service.IGroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/groups")
@RequiredArgsConstructor
public class GroupController {

    private final IGroupService groupService;

    @GetMapping("/{id}")
    public GroupResponse getGroupById(@PathVariable Long id){
        return groupService.getGroup(id);
    }

    @PutMapping("/update/{id}")
    public void updateGroup(@PathVariable Long id, @RequestBody UpdateGroupRequest updateGroupRequest){
        groupService.updateGroup(id, updateGroupRequest);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteGroup (@PathVariable Long id){
        groupService.deleteGroup(id);
    }
}

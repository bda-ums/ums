package com.example.userservice.controller;

import com.example.userservice.model.dto.request.CreateRoleRequest;
import com.example.userservice.model.dto.request.UpdateRoleRequest;
import com.example.userservice.model.dto.response.RoleResponse;
import com.example.userservice.service.IRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/roles")
@RequiredArgsConstructor
public class RoleController {

    private final IRoleService roleService;
    @PostMapping("/add")
    public void createRole(@RequestBody @Valid CreateRoleRequest createRoleRequest){
        roleService.createRole(createRoleRequest);
    }

    @GetMapping("/all")
    public List<RoleResponse> getAllRoles(){
        return roleService.getAllRoles();
    }

    @GetMapping("/{id}")
    public RoleResponse getRoleById(@PathVariable Long id){
        return roleService.getRole(id);
    }

    @PutMapping("/update/{id}")
    public void updateRole(@PathVariable Long id, @RequestBody UpdateRoleRequest updateRoleRequest){
        roleService.updateRole(id, updateRoleRequest);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteRole (@PathVariable Long id){
        roleService.deleteRole(id);
    }

}

package com.example.userservice.controller;

import com.example.userservice.model.dto.response.StudentResponse;
import com.example.userservice.service.impl.StudentServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentServiceImpl studentService;

    @GetMapping("/{id}")
    public StudentResponse getStudentById(@PathVariable Long id){
        return studentService.getStudent(id);
    }


    @DeleteMapping("/delete/{id}")
    public void deleteStudent (@PathVariable Long id){
        studentService.deleteStudent(id);
    }
}

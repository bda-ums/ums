package com.example.userservice.controller;

import com.example.userservice.model.data.Journal;
import com.example.userservice.model.data.JournalDataRequest;
import com.example.userservice.model.data.TaskDTO;
import com.example.userservice.model.dto.response.TeacherResponse;
import com.example.userservice.service.impl.TeacherServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher")
@RequiredArgsConstructor
public class TeacherController {
    private final TeacherServiceImpl teacherService;

    @GetMapping("/{id}")
    public TeacherResponse getTeacherById(@PathVariable Long id){
        return teacherService.getTeacher(id);
    }


    @DeleteMapping("/delete/{id}")
    public void deleteTeacher (@PathVariable Long id){
        teacherService.deleteTeacher(id);
    }

    /**
     * REST TEMPLATE METHODS
     */

    @GetMapping("/journals")
    public List<Journal> getJournals() {
        return teacherService.getJournals();
    }

    @PostMapping("/add-journal-data")
    public void createJournalData(@RequestHeader(name = "id") Long id,
                                  @RequestHeader(name = "groupId") Long groupId,
                                  @RequestHeader(name = "journalId") Long journalId,
                                  @RequestBody JournalDataRequest request) {
        request.setTeacherId(id);
        request.setGroupId(groupId);
        request.setJournalId(journalId);
        teacherService.addJournalData(request);
    }

    @PostMapping("/add-task")
    public void createTaskToGroup(@RequestHeader(name = "id") Long id,
                                  @RequestHeader(name = "groupId") Long groupId,
                                  @RequestBody TaskDTO taskDTO) {
        taskDTO.setTeacherId(id);
        taskDTO.setGroupId(groupId);
        teacherService.addTaskToGroup(taskDTO);
    }

    @PutMapping("/update")
    public void updateTask(@RequestBody TaskDTO taskDTO) {
        teacherService.updateTask(taskDTO);
    }

}

package com.example.userservice.mapper;

import com.example.userservice.model.dto.request.CreateAdminRequest;
import com.example.userservice.model.dto.request.UpdateAdminRequest;
import com.example.userservice.model.dto.response.AdminResponse;
import com.example.userservice.model.entity.Admin;
import org.mapstruct.*;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface AdminMapper {

    @Mapping (target = "id", ignore = true)
    Admin mapCreateAdminRequestToAdmin(CreateAdminRequest adminRequest);

    AdminResponse mapAdminToAdminResponse (Admin admin);

    Admin mapUpdateRequestToEntity(@MappingTarget Admin admin, UpdateAdminRequest request);

}

package com.example.userservice.mapper;

import com.example.userservice.model.dto.request.CreateGroupRequest;
import com.example.userservice.model.dto.request.UpdateGroupRequest;
import com.example.userservice.model.dto.response.GroupResponse;
import com.example.userservice.model.entity.Group;
import org.mapstruct.*;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface GroupMapper {
    @Mapping(target = "id", ignore = true)
    Group mapCreateGroupRequestToGroupEntity (CreateGroupRequest createGroupRequest);

    GroupResponse mapGroupEntityToResponse (Group group);

    Group mapUpdateGroupRequestToEntity (@MappingTarget Group group, UpdateGroupRequest updateGroupRequest);

}

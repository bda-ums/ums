package com.example.userservice.mapper;

import com.example.userservice.model.dto.request.CreateGroupRequest;
import com.example.userservice.model.dto.request.CreateRoleRequest;
import com.example.userservice.model.dto.request.UpdateGroupRequest;
import com.example.userservice.model.dto.request.UpdateRoleRequest;
import com.example.userservice.model.dto.response.GroupResponse;
import com.example.userservice.model.dto.response.RoleResponse;
import com.example.userservice.model.entity.Group;
import com.example.userservice.model.entity.Role;
import org.mapstruct.*;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface RoleMapper {

    @Mapping(target = "id", ignore = true)
    Role mapCreateRoleRequestToRoleEntity (CreateRoleRequest createRoleRequest);

    RoleResponse mapRoleEntityToResponse (Role role);

    Role mapUpdateRoleRequestToEntity (@MappingTarget Role role, UpdateRoleRequest updateRoleRequest);
}

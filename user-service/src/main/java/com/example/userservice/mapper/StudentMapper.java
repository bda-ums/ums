package com.example.userservice.mapper;

import com.example.userservice.model.dto.request.CreateStudentRequest;
import com.example.userservice.model.dto.request.UpdateStudentRequest;
import com.example.userservice.model.dto.response.StudentResponse;
import com.example.userservice.model.entity.Student;
import org.mapstruct.*;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface StudentMapper {
    @Mapping(target = "id", ignore = true)
    Student mapCreateStudentRequestToStudent (CreateStudentRequest createStudentRequest);

    StudentResponse mapEntityToStudentResponse (Student student);

    Student mapUpdateStudentRequestToEntity (@MappingTarget Student student, UpdateStudentRequest updateStudentRequest);
}

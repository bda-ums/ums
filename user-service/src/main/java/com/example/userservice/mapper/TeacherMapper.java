package com.example.userservice.mapper;

import com.example.userservice.model.dto.request.CreateTeacherRequest;
import com.example.userservice.model.dto.request.UpdateTeacherRequest;
import com.example.userservice.model.dto.response.TeacherResponse;
import com.example.userservice.model.entity.Teacher;
import org.mapstruct.*;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface TeacherMapper {
    @Mapping(target = "id", ignore = true)
    Teacher mapCreateTeacherRequestToTeacherEntity (CreateTeacherRequest createTeacherRequest);

    TeacherResponse mapEntityToTeacherResponse (Teacher teacher);

    Teacher mapUpdateTeacherRequestToEntity (@MappingTarget Teacher teacher, UpdateTeacherRequest updateTeacherRequest);


}

package com.example.userservice.model.dto.request;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdateStudentRequest {

    String name;

    String surname;

    String username;

    String email;

    @NotBlank(message = "Password is mandatory")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#$%^&*()-+_]).{8,}$",
            message = "Password must be at least 8 characters long and include at least one lowercase letter, " +
                    "one uppercase letter, one digit, and one special character")
    String password;

    Long groupId;

    Long roleId = 3L; //RoleId-nin update edilməyə ehtiyacı varmı?
}

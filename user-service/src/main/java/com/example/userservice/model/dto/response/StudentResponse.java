package com.example.userservice.model.dto.response;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class StudentResponse {
    Long id;

    String name;

    String surname;

    String username;

    String email;

    String password;

    Long groupId;

    Long roleId;
}

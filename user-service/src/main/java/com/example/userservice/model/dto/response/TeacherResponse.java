package com.example.userservice.model.dto.response;
import com.example.userservice.model.entity.Role;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TeacherResponse {

    Long id;

    String name;

    String surname;

    String username;

    String email;

    String password;

    String subject;

    Role role;

}

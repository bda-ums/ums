package com.example.userservice.model.entity;

import com.example.userservice.security.model.entity.UserEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "otp")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OTP {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String otp;

    @OneToOne
    @JoinColumn(name = "user_id")
    UserEntity user;
}

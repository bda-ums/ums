package com.example.userservice.repository;

import com.example.userservice.model.entity.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRepo extends JpaRepository <Admin, Long> {
}

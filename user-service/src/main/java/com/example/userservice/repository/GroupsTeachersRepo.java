package com.example.userservice.repository;

import com.example.userservice.model.entity.GroupsTeachers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupsTeachersRepo extends JpaRepository<GroupsTeachers, Long> {
}

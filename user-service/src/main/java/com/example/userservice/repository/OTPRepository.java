package com.example.userservice.repository;

import com.example.userservice.model.entity.OTP;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OTPRepository extends JpaRepository<OTP, Long> {
}

package com.example.userservice.security.constant;

public class Text {
    public static String EMAIL_VERIFIED_HTML_TEXT = "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "  <meta charset=\"UTF-8\">\n" +
            "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
            "  <title>Email Verified</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "  <table style=\"width: 100%; max-width: 600px; margin: 0 auto; font-family: Arial, sans-serif; border-collapse: collapse;\">\n" +
            "    <tr>\n" +
            "      <td style=\"padding: 20px; text-align: center; background-color: #4caf50; color: #fff;\">\n" +
            "        <h2>Email Verified</h2>\n" +
            "      </td>\n" +
            "    </tr>\n" +
            "    <tr>\n" +
            "      <td style=\"padding: 20px;\">\n" +
            "        <p>Thank you for verifying your email address. Your account is now active and ready to use.</p>\n" +
            "        <p>If you have any questions or need further assistance, feel free to contact our support team.</p>\n" +
            "      </td>\n" +
            "    </tr>\n" +
            "    <tr>\n" +
            "      <td style=\"padding: 20px; text-align: center; background-color: #4caf50; color: #fff;\">\n" +
            "        <p>&copy; 2023 Detroit University</p>\n" +
            "      </td>\n" +
            "    </tr>\n" +
            "  </table>\n" +
            "</body>\n" +
            "</html>";
}

package com.example.userservice.security.controller;


import com.example.userservice.security.model.dto.request.AuthenticationRequest;
import com.example.userservice.security.model.dto.request.RegistrationRequest;
import com.example.userservice.security.model.dto.response.AuthenticationResponse;
import com.example.userservice.security.service.impl.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static com.example.userservice.security.constant.Text.EMAIL_VERIFIED_HTML_TEXT;


@RestController
@RequiredArgsConstructor
@RequestMapping("/main-auth")
@Slf4j
@CrossOrigin
public class AuthController {
    private final AuthService authService;

    @GetMapping("/resets-token")
    public AuthenticationResponse refreshToken(@RequestHeader(name = HttpHeaders.AUTHORIZATION) String token) {
        return authService.refreshToken(token);
    }

    @PostMapping("/registration")
    public AuthenticationResponse registration(@RequestBody RegistrationRequest request) {
        return authService.registration(request);
    }

    @PostMapping
    public AuthenticationResponse authentication(@RequestBody AuthenticationRequest request) {
        return authService.authentication(request);
    }

//    @GetMapping("/confirm/{uuid}")
//    public ResponseEntity<String> confirmation(@PathVariable UUID uuid) {
//        return authService.confirmAccount(uuid);
//    }

    @GetMapping("/confirm/{uuid}")
    public String confirmation(@PathVariable UUID uuid){
        return EMAIL_VERIFIED_HTML_TEXT;
    }
}

package com.example.userservice.security.helper;

import com.example.userservice.security.model.entity.TokenEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class AuthHelper {
    public TokenEntity buildToken(TokenEntity token, String jwt) {
        return TokenEntity.builder()
                .id(token != null ? token.getId() : null)
                .token(jwt)
                .isExpired(false)
                .isRevoked(false)
                .createdAt(LocalDateTime.now())
                .build();
    }
}

package com.example.userservice.security.helper;

import com.example.userservice.security.model.entity.UserEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

@Service
public class SecurityHelper {

    public boolean isServletPathAuth(@NotNull HttpServletRequest httpServletRequest) {
        return httpServletRequest.getServletPath().contains("/auth");
    }

    public boolean isTokenDead(UserEntity user) {
        return user.getToken().isExpired() || user.getToken().isRevoked();
    }

    public boolean isAuthHeaderValid(String authorizationHeader) {
        return authorizationHeader != null && authorizationHeader.startsWith("Bearer ");
    }

    public boolean isJwtUserFirst(String username) {
        return username != null && SecurityContextHolder.getContext().getAuthentication() == null;
    }
}

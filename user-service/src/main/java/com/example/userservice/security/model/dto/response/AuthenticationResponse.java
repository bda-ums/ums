package com.example.userservice.security.model.dto.response;

import com.example.userservice.security.model.dto.request.AuthenticationRequest;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthenticationResponse {
    String message;
    String accessToken;
    String refreshToken;
    String role;
    String name;
    String surname;
    AuthenticationRequest user;
}

package com.example.userservice.security.model.enums;

public enum RoleType {
    ADMIN("ADMIN"),
    TEACHER("TEACHER"),
    STUDENT("STUDENT");

    public final String value;

    RoleType(String value) {
        this.value = value;
    }
}

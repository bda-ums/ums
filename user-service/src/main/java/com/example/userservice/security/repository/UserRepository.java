package com.example.userservice.security.repository;

import com.example.userservice.security.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Query(value = "select * from _user where username = :username", nativeQuery = true)
    Optional<UserEntity> findUserByUsernameOrEmail(@Param("username") String username);
}

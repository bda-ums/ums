package com.example.userservice.security.service;


import com.example.userservice.security.model.dto.request.AuthenticationRequest;
import com.example.userservice.security.model.dto.request.RegistrationRequest;
import com.example.userservice.security.model.dto.response.AuthenticationResponse;

public interface IAuthService {
    AuthenticationResponse registration(RegistrationRequest request);

    AuthenticationResponse authentication(AuthenticationRequest authenticationRequest);

    AuthenticationResponse refreshToken(String authHeader);
}

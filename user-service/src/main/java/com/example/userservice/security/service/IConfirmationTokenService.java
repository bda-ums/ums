package com.example.userservice.security.service;


import com.example.userservice.security.model.dto.response.ResponseDto;
import com.example.userservice.security.model.entity.ConfirmationTokenEntity;

public interface IConfirmationTokenService {
    ResponseDto save(ConfirmationTokenEntity confirmationTokenEntity);

    ConfirmationTokenEntity getTokenByUUID(String uuid);
}
package com.example.userservice.security.service.impl;


import com.example.userservice.model.dto.request.ConfirmationMailRequest;
import com.example.userservice.model.entity.OTP;
import com.example.userservice.security.helper.SecurityHelper;
import com.example.userservice.security.model.dto.request.AuthenticationRequest;
import com.example.userservice.security.model.dto.request.RegistrationRequest;
import com.example.userservice.security.model.dto.response.AuthenticationResponse;
import com.example.userservice.security.model.entity.ConfirmationTokenEntity;
import com.example.userservice.security.model.entity.RoleEntity;
import com.example.userservice.security.model.entity.TokenEntity;
import com.example.userservice.security.model.entity.UserEntity;
import com.example.userservice.security.repository.RoleRepository;
import com.example.userservice.security.repository.TokenRepository;
import com.example.userservice.security.repository.UserRepository;
import com.example.userservice.security.service.IAuthService;
import com.example.userservice.security.service.IConfirmationTokenService;
import com.example.userservice.service.IOTP;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
@Slf4j
@RequiredArgsConstructor
public class AuthService implements IAuthService {
    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final RoleRepository roleRepository;
    private final JwtService jwtService;
    private final SecurityHelper securityHelper;
    private final TokenRepository tokenRepository;
    private final PasswordEncoder passwordEncoder;
    private final IConfirmationTokenService confirmationTokenService;
    private final IOTP otpService;
    private final RestTemplate restTemplate;
//    private final AdminRepository adminRepository;
//    private final StudentRepository studentRepository;
//    private final TeacherRepository teacherRepository;

    private UserEntity getUser(RegistrationRequest request, RoleEntity role) {
        UserEntity user = UserEntity.builder()
                .name(request.getName())
                .surname(request.getSurname())
                .email(request.getEmail())
                .username(request.getUsername())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(role)
                .build();
        userRepository.save(user);

        //Send Mail
        return user;
    }

    private TokenEntity getToken(RegistrationRequest request, String accessToken) {
        Optional<UserEntity> _user = userRepository.findUserByUsernameOrEmail(request.getUsername());

        return TokenEntity.builder()
                .token(accessToken)
                .isRevoked(false)
                .isExpired(false)
                .user(_user.orElseThrow())
                .createdAt(LocalDateTime.now())
                .build();
    }

    public ResponseEntity<String> confirmAccount(UUID uuid) {
        ConfirmationTokenEntity token = confirmationTokenService.getTokenByUUID(uuid.toString());
        if (token != null) {
            UserEntity user = token.getUser();
            user.set_enable(true);
            userRepository.save(user);

            token.setConfirmedAt(LocalDateTime.now());
            confirmationTokenService.save(token);
            return ResponseEntity.ok().body(user.getEmail() + "Confirmation is successfully");
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Link is invalid");
        }
    }

    public void addStaticRolesToRoleTable() {
        List<RoleEntity> listRoles = roleRepository.findAll();
        if (listRoles.isEmpty()) {
            RoleEntity roleAdmin = new RoleEntity(null, "ADMIN", null);
            RoleEntity roleTeacher = new RoleEntity(null, "TEACHER", null);
            RoleEntity roleStudent = new RoleEntity(null, "STUDENT", null);
            roleRepository.save(roleAdmin);
            roleRepository.save(roleTeacher);
            roleRepository.save(roleStudent);
        }
    }

    @Override
    public AuthenticationResponse registration(RegistrationRequest request) {
        try {
            addStaticRolesToRoleTable();
        } catch (Exception e) {
            throw new RuntimeException("rollar elave oluna bilmedi");
        }

        RoleEntity role = roleRepository.findRoleByName(request.getRole().toUpperCase())
                .orElseThrow(() -> new RuntimeException("role not found"));

        UserEntity user = getUser(request, role);

        String accessToken = jwtService.generateToken(user);
        String refreshToken = jwtService.generateRefreshToken(user);

        TokenEntity token = getToken(request, accessToken);
        tokenRepository.save(token);
        user.setToken(token);
        userRepository.save(user);


      OTP otp =  OTP.builder()
                .user(user)
                .otp(UUID.randomUUID().toString())
                .build();

        ConfirmationMailRequest confirmationMailRequest = ConfirmationMailRequest.builder()
                .email(request.getEmail())
                .otp(otp.getOtp())
                .username(request.getUsername())
                .password(request.getPassword())
                .build();
      otpService.save(confirmationMailRequest,user);
      restTemplate.postForEntity("http://localhost:8081/mail/send/confirm-mail",confirmationMailRequest, ConfirmationMailRequest.class);

        return AuthenticationResponse.builder()
                .message("A confirmation email was sent to " + user.getEmail() + ". Please verify your account by clicking on the link")
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .build();
    }

    @Override
    public AuthenticationResponse authentication(AuthenticationRequest request) {
        log.info("Entered is authenticate method");
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            request.getUsername(),
                            request.getPassword()
                    )
            );
        } catch (Exception e) {
            log.error("Authentication failed: {}", e.getMessage());
            throw new RuntimeException("Authentication failed", e);
        }
        log.info("Method exited");
        Optional<UserEntity> user = userRepository.findUserByUsernameOrEmail(request.getUsername());
        String accessToken = jwtService.generateToken(user.orElseThrow());
        String refreshToken = jwtService.generateRefreshToken(user.orElseThrow());
        Optional<UserEntity> _user = userRepository.findUserByUsernameOrEmail(request.getUsername());
        Optional<String> role = _user.map((item) -> item.getRole().getName());
        String roleString = role.get().toString();
        String name = user.map(UserEntity::getName).get().toString();
        String surname = user.map(UserEntity::getSurname).get().toString();

        TokenEntity token = tokenRepository.findTokenByUser(user.orElseThrow())
                .orElseThrow(() -> new RuntimeException("Token doesn`t exist: " + user));
        token.setToken(accessToken);
        tokenRepository.save(token);

        return AuthenticationResponse.builder()
                .user(new AuthenticationRequest(user.orElseThrow().getUsername(), user.orElseThrow().getPassword()))
                .message(user.orElseThrow().getEmail() + " loginn is successfully")
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .role(roleString)
                .name(name)
                .surname(surname)
                .build();
    }

    @Override
    public AuthenticationResponse refreshToken(String authHeader) {
        if (!securityHelper.isAuthHeaderValid(authHeader)) {
            throw new RuntimeException();
        }

        String jwt = authHeader.substring(7);
        String username = jwtService.extractUsername(jwt);

        if (username != null) {
            UserEntity user = userRepository.findUserByUsernameOrEmail(username)
                    .orElseThrow(() -> new RuntimeException("Username doesn`t exist: " + username));
            TokenEntity token = tokenRepository.findTokenByUser(user)
                    .orElseThrow(() -> new RuntimeException("Token doesn`t exist: " + user));

            if (jwtService.isTokenValid(jwt, user)) {
                String accessToken = jwtService.generateToken(user);
                String refreshToken = jwtService.generateRefreshToken(user);

                token.setToken(accessToken);
                tokenRepository.save(token);
                return AuthenticationResponse.builder()
                        .message(username + " refreshing is successfully")
                        .accessToken(accessToken)
                        .refreshToken(refreshToken)
                        .build();
            }

        }
        throw new RuntimeException("token is invalid!");
    }
}

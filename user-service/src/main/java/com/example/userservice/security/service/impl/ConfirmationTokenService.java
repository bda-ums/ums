package com.example.userservice.security.service.impl;


import com.example.userservice.exception.ApplicationException;
import com.example.userservice.security.model.dto.response.ResponseDto;
import com.example.userservice.security.model.entity.ConfirmationTokenEntity;
import com.example.userservice.security.model.enums.Exceptions;
import com.example.userservice.security.repository.ConfirmationTokenRepository;
import com.example.userservice.security.service.IConfirmationTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class ConfirmationTokenService implements IConfirmationTokenService {
    private final ConfirmationTokenRepository confirmationTokenRepository;

    @Override
    public ResponseDto save(ConfirmationTokenEntity confirmationTokenEntity) {
        ConfirmationTokenEntity save = confirmationTokenRepository.save(confirmationTokenEntity);
        if (save != null) {
            return new ResponseDto("Save is successfull");
        } else {
            throw new ApplicationException(Exceptions.TOKEN_IS_INVALID_EXCEPTION);
        }
    }

    @Override
    public ConfirmationTokenEntity getTokenByUUID(String uuid) {
        return confirmationTokenRepository.findConfirmationTokenByToken(uuid)
                .orElseThrow(() -> new ApplicationException(Exceptions.TOKEN_NOT_FOUND_EXCEPTION));
    }
}

/*
 *
 * http://localhost:8080/auth/registration
 *
 * */
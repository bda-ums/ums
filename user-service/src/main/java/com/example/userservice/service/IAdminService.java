package com.example.userservice.service;


import com.example.userservice.model.dto.request.CreateAdminRequest;
import com.example.userservice.model.dto.response.AdminResponse;

import java.util.List;

public interface IAdminService {

    void createAdmin(CreateAdminRequest adminRequest);
    List<AdminResponse> getAdmins();

}

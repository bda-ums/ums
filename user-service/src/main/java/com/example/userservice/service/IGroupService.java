package com.example.userservice.service;

import com.example.userservice.exception.NotFoundException;
import com.example.userservice.model.dto.request.CreateGroupRequest;
import com.example.userservice.model.dto.request.UpdateGroupRequest;
import com.example.userservice.model.dto.response.GroupResponse;

import java.util.List;

public interface IGroupService {
    void createGroup(CreateGroupRequest createGroupRequest);

    void deleteGroup(Long id);

    void updateGroup(Long id, UpdateGroupRequest updateGroupRequest);

    List<GroupResponse> getAllGroups();

    GroupResponse getGroup(Long id);

    NotFoundException exGroupNotFound(Long id);
}

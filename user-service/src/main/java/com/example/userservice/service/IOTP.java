package com.example.userservice.service;

import com.example.userservice.model.dto.request.ConfirmationMailRequest;
import com.example.userservice.security.model.entity.UserEntity;

public interface IOTP {

    void save(ConfirmationMailRequest confirmationMailRequest, UserEntity user);
}

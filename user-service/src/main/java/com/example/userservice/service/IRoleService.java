package com.example.userservice.service;

import com.example.userservice.exception.NotFoundException;
import com.example.userservice.model.dto.request.CreateGroupRequest;
import com.example.userservice.model.dto.request.CreateRoleRequest;
import com.example.userservice.model.dto.request.UpdateGroupRequest;
import com.example.userservice.model.dto.request.UpdateRoleRequest;
import com.example.userservice.model.dto.response.GroupResponse;
import com.example.userservice.model.dto.response.RoleResponse;

import java.util.List;

public interface IRoleService {
    void createRole(CreateRoleRequest createRoleRequest);

    void deleteRole(Long id);

    void updateRole(Long id, UpdateRoleRequest updateRoleRequest);

    List<RoleResponse> getAllRoles();

    RoleResponse getRole(Long id);

    NotFoundException exRoleNotFound(Long id);
}

package com.example.userservice.service;

import com.example.userservice.exception.NotFoundException;
import com.example.userservice.model.dto.request.CreateStudentRequest;
import com.example.userservice.model.dto.request.UpdateStudentRequest;
import com.example.userservice.model.dto.response.StudentResponse;

import java.util.List;

public interface IStudentService {
    void createStudent(CreateStudentRequest createStudentRequest);

    void deleteStudent(Long id);

    void updateStudent(Long id, UpdateStudentRequest updateStudentRequest);

    List<StudentResponse> getAllStudents();

    StudentResponse getStudent(Long id);

    NotFoundException exStudentNotFound(Long id);
}

package com.example.userservice.service;

import com.example.userservice.exception.NotFoundException;
import com.example.userservice.model.dto.request.CreateTeacherRequest;
import com.example.userservice.model.dto.request.UpdateTeacherRequest;
import com.example.userservice.model.dto.response.TeacherResponse;

import java.util.List;

public interface ITeacherService {
    void createTeacher(CreateTeacherRequest createTeacherRequest);

    void deleteTeacher(Long id);

    void updateTeacher(Long id, UpdateTeacherRequest updateTeacherRequest);

    List<TeacherResponse> getAllTeachers();

    TeacherResponse getTeacher(Long id);

    NotFoundException exTeacherNotFound(Long id);

  }

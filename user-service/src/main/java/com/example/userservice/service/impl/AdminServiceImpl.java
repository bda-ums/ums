package com.example.userservice.service.impl;

import com.example.userservice.mapper.AdminMapper;
import com.example.userservice.model.dto.request.CreateAdminRequest;
import com.example.userservice.model.dto.response.AdminResponse;
import com.example.userservice.repository.AdminRepo;
import com.example.userservice.security.model.dto.request.RegistrationRequest;
import com.example.userservice.security.service.impl.AuthService;
import com.example.userservice.service.IAdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor

public class AdminServiceImpl implements IAdminService {

    private final AdminRepo adminRepo;
    private final AdminMapper adminMapper;
    private final AuthService authService;


    @Override
    public void createAdmin(CreateAdminRequest request) {
        request.setRoleId(1L);
        String role = "ADMIN";
        adminRepo.save(adminMapper.mapCreateAdminRequestToAdmin(request));
        RegistrationRequest registrationRequest = new RegistrationRequest(request.getName(), request.getSurname(), request.getUsername(), request.getEmail(), request.getPassword(), role, 25L);
        authService.registration(registrationRequest);

    }

    @Override
    public List<AdminResponse> getAdmins() {
        return adminRepo.findAll()
                .stream().map(adminMapper::mapAdminToAdminResponse).collect(Collectors.toList());
    }


}

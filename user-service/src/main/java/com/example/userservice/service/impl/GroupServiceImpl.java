package com.example.userservice.service.impl;

import com.example.userservice.exception.NotFoundException;
import com.example.userservice.mapper.GroupMapper;
import com.example.userservice.model.dto.request.CreateGroupRequest;
import com.example.userservice.model.dto.request.UpdateGroupRequest;
import com.example.userservice.model.dto.response.GroupResponse;
import com.example.userservice.repository.GroupRepo;
import com.example.userservice.service.IGroupService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class GroupServiceImpl implements IGroupService {

    private final GroupRepo groupRepo;
    private final GroupMapper groupMapper;

    @Override
    public void createGroup(CreateGroupRequest createGroupRequest) {
        groupRepo.save(groupMapper.mapCreateGroupRequestToGroupEntity(createGroupRequest));
    }

    @Override
    public void deleteGroup(Long id) {
        var group = groupRepo.findById(id)
                .orElseThrow(() -> exGroupNotFound(id));
        groupRepo.deleteById(group.getId());
    }

    @Override
    public void updateGroup(Long id, UpdateGroupRequest updateGroupRequest) {
        var group = groupRepo.findById(id)
                .orElseThrow(() -> exGroupNotFound(id));

        groupMapper.mapUpdateGroupRequestToEntity(group, updateGroupRequest);
        groupRepo.save(group);

    }

    @Override
    public List<GroupResponse> getAllGroups() {
        return groupRepo.findAll()
                .stream()
                .map(groupMapper::mapGroupEntityToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public GroupResponse getGroup(Long id) {
        var group = groupRepo.findById(id)
                .orElseThrow(() -> exGroupNotFound(id));
        return groupMapper.mapGroupEntityToResponse(group);
    }

    @Override
    public NotFoundException exGroupNotFound(Long id) {
        throw new NotFoundException("Group not found by id " + id);
    }
}

package com.example.userservice.service.impl;

import com.example.userservice.model.dto.request.ConfirmationMailRequest;
import com.example.userservice.model.entity.OTP;
import com.example.userservice.repository.OTPRepository;
import com.example.userservice.security.model.entity.UserEntity;
import com.example.userservice.service.IOTP;
import org.springframework.stereotype.Service;

@Service
public class OTPService implements IOTP {

    private final OTPRepository otpRepository;

    public OTPService(OTPRepository otpRepository) {
        this.otpRepository = otpRepository;
    }

    @Override
    public void save(ConfirmationMailRequest confirmationMailRequest, UserEntity user) {
        OTP otp = new OTP(null, confirmationMailRequest.getOtp(), user);
        otpRepository.save(otp);
    }
}

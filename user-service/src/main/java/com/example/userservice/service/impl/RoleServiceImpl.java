package com.example.userservice.service.impl;

import com.example.userservice.exception.NotFoundException;
import com.example.userservice.mapper.GroupMapper;
import com.example.userservice.mapper.RoleMapper;
import com.example.userservice.model.dto.request.CreateRoleRequest;
import com.example.userservice.model.dto.request.UpdateRoleRequest;
import com.example.userservice.model.dto.response.RoleResponse;
import com.example.userservice.repository.GroupRepo;
import com.example.userservice.repository.RoleRepo;
import com.example.userservice.service.IRoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class RoleServiceImpl implements IRoleService {

    private final RoleRepo roleRepo;
    private final RoleMapper roleMapper;


    @Override
    public void createRole(CreateRoleRequest createRoleRequest) {
        roleRepo.save(roleMapper.mapCreateRoleRequestToRoleEntity(createRoleRequest));
    }

    @Override
    public void deleteRole(Long id) {
        var role = roleRepo.findById(id)
                .orElseThrow(() -> exRoleNotFound(id));
        roleRepo.deleteById(role.getId());

    }

    @Override
    public void updateRole(Long id, UpdateRoleRequest updateRoleRequest) {
        var role = roleRepo.findById(id)
                .orElseThrow(() -> exRoleNotFound(id));

        roleMapper.mapUpdateRoleRequestToEntity(role, updateRoleRequest);
        roleRepo.save(role);


    }

    @Override
    public List<RoleResponse> getAllRoles() {
        return roleRepo.findAll()
                .stream()
                .map(roleMapper::mapRoleEntityToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public RoleResponse getRole(Long id) {
        var role = roleRepo.findById(id)
                .orElseThrow(() -> exRoleNotFound(id));
        return roleMapper.mapRoleEntityToResponse(role);

    }

    @Override
    public NotFoundException exRoleNotFound(Long id) {
        throw new NotFoundException("Role not found by id " + id);
    }
}

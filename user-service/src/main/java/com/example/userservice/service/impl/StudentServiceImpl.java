package com.example.userservice.service.impl;

import com.example.userservice.exception.NotFoundException;
import com.example.userservice.mapper.StudentMapper;
import com.example.userservice.model.dto.request.CreateStudentRequest;
import com.example.userservice.model.dto.request.UpdateStudentRequest;
import com.example.userservice.model.dto.response.StudentResponse;
import com.example.userservice.repository.StudentRepo;
import com.example.userservice.security.model.dto.request.RegistrationRequest;
import com.example.userservice.security.service.impl.AuthService;
import com.example.userservice.service.IStudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentServiceImpl implements IStudentService {

    private final StudentRepo studentRepo;
    private final StudentMapper studentMapper;
    private final AuthService authService;
    @Override
    public void createStudent(CreateStudentRequest request) {
        request.setRoleId(3L);
        String role = "STUDENT";
        studentRepo.save(studentMapper.mapCreateStudentRequestToStudent(request));
        RegistrationRequest registrationRequest = new RegistrationRequest(request.getName(), request.getSurname(), request.getUsername(), request.getEmail(), request.getPassword(), role, 25L);
        authService.registration(registrationRequest);
    }

    @Override
    public void deleteStudent(Long id) {
        var student = studentRepo.findById(id)
                .orElseThrow(() -> exStudentNotFound(id));
        studentRepo.deleteById(student.getId());
    }

    @Override
    public void updateStudent(Long id, UpdateStudentRequest updateStudentRequest) {
        var student = studentRepo.findById(id)
                .orElseThrow(() -> exStudentNotFound(id));

        studentMapper.mapUpdateStudentRequestToEntity(student, updateStudentRequest);
        studentRepo.save(student);
    }

    @Override
    public List<StudentResponse> getAllStudents() {
        return studentRepo.findAll()
                .stream()
                .map(studentMapper::mapEntityToStudentResponse)
                .collect(Collectors.toList());
    }

    @Override
    public StudentResponse getStudent(Long id) {
        var student = studentRepo.findById(id)
                .orElseThrow(() -> exStudentNotFound(id));

        return studentMapper.mapEntityToStudentResponse(student);
    }

    @Override
    public NotFoundException exStudentNotFound(Long id) {
        throw new NotFoundException("Student not found by id " + id);
    }


}

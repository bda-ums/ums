package com.example.userservice.service.impl;

import com.example.userservice.exception.NotFoundException;
import com.example.userservice.mapper.TeacherMapper;
import com.example.userservice.model.data.Journal;
import com.example.userservice.model.data.JournalDataRequest;
import com.example.userservice.model.data.TaskDTO;
import com.example.userservice.model.dto.request.CreateTeacherRequest;
import com.example.userservice.model.dto.request.UpdateTeacherRequest;
import com.example.userservice.model.dto.response.TeacherResponse;
import com.example.userservice.repository.TeacherRepo;
import com.example.userservice.security.model.dto.request.RegistrationRequest;
import com.example.userservice.security.service.impl.AuthService;
import com.example.userservice.service.ITeacherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TeacherServiceImpl implements ITeacherService {

    private final TeacherRepo teacherRepo;
    private final TeacherMapper teacherMapper;
    private final AuthService authService;

    private final RestTemplate restTemplate;

    public TeacherServiceImpl(TeacherRepo teacherRepo,
                              TeacherMapper teacherMapper,
                              AuthService authService,
                              RestTemplate restTemplate) {
        this.teacherRepo = teacherRepo;
        this.teacherMapper = teacherMapper;
        this.authService = authService;
        this.restTemplate = restTemplate;
    }

    @Override
    public void createTeacher(CreateTeacherRequest request) {
        request.setRoleId(2L);
        String role = "TEACHER";
        teacherRepo.save(teacherMapper.mapCreateTeacherRequestToTeacherEntity(request));
        RegistrationRequest registrationRequest = new RegistrationRequest(request.getName(), request.getSurname(), request.getUsername(), request.getEmail(), request.getPassword(), role, 25L);
        authService.registration(registrationRequest);
    }

    @Override
    public void deleteTeacher(Long id) {
        var teacher = teacherRepo.findById(id)
                .orElseThrow(() -> exTeacherNotFound(id));

        teacherRepo.deleteById(teacher.getId());
    }

    @Override
    public void updateTeacher(Long id, UpdateTeacherRequest updateTeacherRequest) {
        var teacher = teacherRepo.findById(id)
                .orElseThrow(() -> exTeacherNotFound(id));

        teacherMapper.mapUpdateTeacherRequestToEntity(teacher, updateTeacherRequest);
    }

    @Override
    public List<TeacherResponse> getAllTeachers() {
        return teacherRepo.findAll()
                .stream()
                .map(teacherMapper::mapEntityToTeacherResponse)
                .collect(Collectors.toList());
    }

    @Override
    public TeacherResponse getTeacher(Long id) {
        var teacher = teacherRepo.findById(id)
                .orElseThrow(() -> exTeacherNotFound(id));

        return teacherMapper.mapEntityToTeacherResponse(teacher);
    }

    @Override
    public NotFoundException exTeacherNotFound(Long id) {
        throw new NotFoundException("Teacher not found by id" + id);
    }


    /**
     * REST TEMPLATE METODLARI
     */

    public List<Journal> getJournals() {
        Journal[] objects = restTemplate.getForObject("http://localhost:8085/journal/all", Journal[].class);
        System.out.println(Arrays.toString(objects));
        assert objects != null;
        return Arrays.asList(objects);
    }

    public void addJournalData(JournalDataRequest request) {
        restTemplate.postForEntity("http://localhost:8085/journaldata", request, JournalDataRequest.class);
    }

    public void addTaskToGroup(TaskDTO taskDTO) {
        restTemplate.postForEntity("http://localhost:8082/task/add", taskDTO, TaskDTO.class);
    }

    public void updateTask(TaskDTO taskDTO) {
        restTemplate.put("http://localhost:8082/task/update", taskDTO, TaskDTO.class);
    }


}
